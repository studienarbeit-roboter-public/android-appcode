package de.dhbw.moro.robot;

import android.util.Log;

import de.dhbw.moro.datatypes.Position;

/**
 * Singleton Pattern
 */
public class Robot {
    private boolean connectionState;
    private Position position;
    private static Robot instance = null;

    private Robot() {
        this.position = new Position();
        connectionState = false;
    }

    /**
     * getter
     * @return the instance
     */
    public static Robot getInstance() {
        if (instance == null) {
            instance = new Robot();
            //set a default position
            instance.setPosition(49.50741, 8.49189, 300);
        }
        return instance;
    }

    public boolean setPosition(double lat, double lon, double course) {
        position.setLatLonCourse(lat, lon, course);
        return position.isValid();
    }

    public boolean setPosition(Position newPos) {
        position.setLatLonCourse(newPos.getLatitude(), newPos.getLongitude(), newPos.getCourse());
        Log.i("GPSHandler", "setPosition"+position.toString());
        return position.isValid();
    }

    public Position getPosition(){
        return position;
    }
}
