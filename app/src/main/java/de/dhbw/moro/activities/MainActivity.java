package de.dhbw.moro.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton;

import de.dhbw.moro.R;
import de.dhbw.moro.communication.control.DriveControl;
import de.dhbw.moro.communication.gps.GPSHandler;
import de.dhbw.moro.map.Map;

import com.google.android.gms.maps.SupportMapFragment;

public class MainActivity extends AppCompatActivity{

    private static boolean first_gps_start = true;

    RelativeLayout layout_landscape;
    SeekBar sb_slider_accelerate;

    ToggleButton bt_toggle_map;
    ToggleButton bt_follow_map;

    Map map_object = Map.getInstance();

    DriveControl driveControl = new DriveControl("MoRo");
    boolean driveDirectionForward = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(map_object);


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_start, null);
        CheckBox mCheckBox = mView.findViewById(R.id.checkBox);
        mBuilder.setTitle(R.string.dialog_title);
        mBuilder.setView(mView);
        mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    storeDialogStatus(true);
                }else{
                    storeDialogStatus(false);
                }
            }
        });

        if(getDialogStatus()){
            mDialog.hide();
        }else{
            mDialog.show();
        }


        //make sure that the slider is as wide as the app
        layout_landscape =  (RelativeLayout)findViewById(R.id.main_landscape);
        sb_slider_accelerate = (SeekBar)findViewById(R.id.seekBar_vertical);
        //layout_landscape is null if the app is in portrait mode
        if (layout_landscape!=null){
            map_object.set_portrait(false);
            initButtons();
            ViewTreeObserver viewTreeObs = layout_landscape.getViewTreeObserver();
            viewTreeObs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int height = layout_landscape.getHeight();
                    ViewGroup.LayoutParams params = sb_slider_accelerate.getLayoutParams();
                    params.width=height-50; //-50 for the margin at bottom und top of display
                    sb_slider_accelerate.setLayoutParams(params);
                }
            });
        }
        else{
            map_object.set_portrait(true);
        }
        if(sb_slider_accelerate!=null) {
            //set what to do when the slider is moved
            sb_slider_accelerate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                int progressChangedValue = 0;
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    progressChangedValue = progress;
                    Log.i("Progress", String.valueOf(progress));
                }
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }
                public void onStopTrackingTouch(SeekBar seekBar) {
                    ;
                    String message = getResources().getString(R.string.info_progres) + String.valueOf(progressChangedValue);
                    Toast.makeText(MainActivity.this, message,
                            Toast.LENGTH_SHORT).show();
                    if (progressChangedValue == 0) {
                        driveControl.stop();
                    }
                    else {
                        if (driveDirectionForward) {
                            driveControl.forwardAllWheels((short)progressChangedValue);
                        }
                        if (!driveDirectionForward) {
                            driveControl.backwardAllWheels((short)progressChangedValue);
                        }
                    }

                }
            });
        }

    }


    private void storeDialogStatus(boolean isChecked){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("item", isChecked);
        mEditor.apply();
    }

    private boolean getDialogStatus(){
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("item", false);
    }


    private void initButtons() {

        bt_follow_map = (ToggleButton) findViewById(R.id.toggle_map_follow);
        if (map_object.get_follow_marker()){
            bt_follow_map.setChecked(true);
        }
        bt_follow_map.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    map_object.set_follow_marker(true);
                }
                else{
                    map_object.set_follow_marker(false);
                }
            }
        });

        bt_toggle_map = (ToggleButton) findViewById(R.id.toggle_map_on_off);
        if (!map_object.get_is_visible()){
            FrameLayout map_layout = (FrameLayout)findViewById(R.id.mapLayout);
            bt_toggle_map.setChecked(true);
            map_layout.setVisibility(View.GONE);
        }
        bt_toggle_map.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FrameLayout map_layout = (FrameLayout)findViewById(R.id.mapLayout);
                if(isChecked){
                    map_layout.setVisibility(View.GONE);
                    map_object.set_is_visible(false);
                }
                else{
                    map_layout.setVisibility(View.VISIBLE);
                    map_object.set_is_visible(true);
                }
            }
        });

        ImageButton left = (ImageButton) findViewById(R.id.button_left);
        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                driveControl.left();
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    int progressChangedValue = sb_slider_accelerate.getProgress();
                    if (progressChangedValue == 0) {
                        driveControl.stop();
                    }
                    else {
                        if (driveDirectionForward) {
                            driveControl.forwardAllWheels((short)progressChangedValue);
                        }
                        if (!driveDirectionForward) {
                            driveControl.backwardAllWheels((short)progressChangedValue);
                        }
                    }
                }
                return true;
            }
        });


        ImageButton right = (ImageButton) findViewById(R.id.button_right);
        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                driveControl.right();
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    int progressChangedValue = sb_slider_accelerate.getProgress();
                    if (progressChangedValue == 0) {
                        driveControl.stop();
                    }
                    else {
                        if (driveDirectionForward) {
                            driveControl.forwardAllWheels((short)progressChangedValue);
                        }
                        if (!driveDirectionForward) {
                            driveControl.backwardAllWheels((short)progressChangedValue);
                        }
                    }
                }
                return true;
            }
        });


        final ToggleButton gear = (ToggleButton) findViewById(R.id.button_gear);
        gear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sb_slider_accelerate.getProgress() == 0) {
                    if (gear.isChecked()) {
                        driveDirectionForward = true;
                    } else {
                        driveDirectionForward = false;
                    }
                }
                else {
                    Toast.makeText(MainActivity.this, R.string.info_schubregler,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        // START GPSHandler
        if (first_gps_start) {
            Log.i("MainActivity", "GPSHandler gestartet");
            GPSHandler gps = new GPSHandler();
            gps.start();
            first_gps_start=false;
        }
    }

}
