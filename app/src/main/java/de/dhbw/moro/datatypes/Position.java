package de.dhbw.moro.datatypes;

import java.util.Date;

/**
 * datatype to save the GPS position
 */
public class Position {
    private double latitude;
    private double longitude;
    private double course;
    private Date update;
    private boolean valid;

    /**
     * constructor
     */
    public Position() {
        this.valid = false;
        this.update = new Date(System.currentTimeMillis());
    }

    /**
     * set the position
     * @param lat latitude coordinate
     * @param lon longitude coordinate
     * @param course the course
     */
    public void setLatLonCourse(double lat, double lon, double course) {
        this.latitude = lat;
        this.longitude = lon;
        this.course = course;
        update.setTime(System.currentTimeMillis());
        valid = true;
    }

    /**
     * getter
     * @return the latitude coordinate
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * getter
     * @return the longitude coordinate
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * getter
     * @return the course
     */
    public double getCourse() {
        return this.course;
    }

    /**
     * check whether position is valid
     * @return valid - true, invalid - false
     */
    public boolean isValid() {
        return this.valid;
    }

    /**
     * getter
     * @return last position update
     */
    public Date getLastUpdate() {
        return this.update;
    }

    /**
     * make position invalid
     */
    public void setInvalid() {
        this.valid = false;
    }

    @Override
    public String toString() {
        return "Pos: " + getLatitude() + ", " + getLongitude() + ", " + getCourse() + "°, Last Update: " + getLastUpdate().toString();
    }
}
