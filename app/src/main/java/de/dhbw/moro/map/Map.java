package de.dhbw.moro.map;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

import de.dhbw.moro.R;
import de.dhbw.moro.robot.Robot;

public class Map implements OnMapReadyCallback {
    private Robot robot = Robot.getInstance();
    private boolean is_visible = true;
    private boolean follow_marker = false;
    private boolean portrait_map = true;

    private GoogleMap mMap;
    Marker current_position;
    private static Map instance = null;

    private Map(){}

    private MyLocationSource mLocationSource; //for using the blue dot

    public static Map getInstance(){
        if (instance == null) {
            instance = new Map();
        }
        return instance;
    }

    //this is to use the blue dot to signal our location
    //im Beispiel war die Klasse static. Das ging aber nicht wenn ich Robot verwenden will -> vielleicht eine Fehlerquelle
    private class MyLocationSource extends Thread implements LocationSource {

        private OnLocationChangedListener mListener;
        private boolean mPaused;
        private boolean first = true;

        @Override
        public void activate(OnLocationChangedListener listener) {
            mListener = listener;
        }

        @Override
        public void deactivate() {
            mListener = null;
        }

        @Override
        public void run() {
            if (first) {
                Handler handler1 = new Handler(Looper.getMainLooper());
                handler1.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("handler1", "running");
                        LatLng new_pos = new LatLng(robot.getPosition().getLatitude(), robot.getPosition().getLongitude());
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new_pos, 15));
                    }
                });
                first=false;
            }

            while(true) {
                if (mListener != null && !mPaused) {
                    Location location = new Location("LongPressLocationProvider");
                    location.setLatitude(robot.getPosition().getLatitude());
                    location.setLongitude(robot.getPosition().getLongitude());
                    location.setBearing((float) robot.getPosition().getCourse());
                    mListener.onLocationChanged(location);
                    //brauche MainLooper um die Kameraposisition neu zu setzen
                    //eventuell ist hier eine Fehlerquelle
                    //im portrait Modus folgen wir immer dem Roboter
                    if (follow_marker||portrait_map) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("Maps", "doing the Action on MainEvent");
                                LatLng new_pos = new LatLng(robot.getPosition().getLatitude(), robot.getPosition().getLongitude());
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(new_pos));
                            }
                        });
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public void onPause() {
            mPaused = true;
        }

        public void onResume() {
            mPaused = false;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //for using a custom marker image
        //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.moro);


        //for using the blue dot
        mLocationSource = new MyLocationSource();
        mLocationSource.start();
        mMap.setLocationSource(mLocationSource);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        /*
        // Add a marker in default position and move the camera
        LatLng default_pos = new LatLng(robot.getPosition().getLatitude(), robot.getPosition().getLongitude());
        current_position = mMap.addMarker(new MarkerOptions().position(default_pos).title("MoRo"));
        //current_position = mMap.addMarker(new MarkerOptions().position(default_pos).title("MoRo").icon(icon));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(default_pos, 15));

        UpdateThread updateThread = new UpdateThread();
        updateThread.execute();


         */
    }
    public void set_portrait(boolean visible){
        this.portrait_map=visible;
    }

    public void set_is_visible(boolean visible){
        this.is_visible=visible;
    }

    public boolean get_is_visible(){
        return is_visible;
    }

    public void set_follow_marker(boolean follow){
        this.follow_marker=follow;
    }

    public boolean get_follow_marker(){
        return follow_marker;
    }

    private class UpdateThread extends AsyncTask<Void, LatLng, Void>
    {
        @Override
        protected void onProgressUpdate(final LatLng... values) {
            Handler handler = new Handler(Looper.getMainLooper());
            super.onProgressUpdate(values);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    //LatLng new_pos = new LatLng(robot.getPosition().getLatitude(), robot.getPosition().getLongitude());
                    current_position.setPosition(values[0]);
                    //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new_pos, 15));

                    //im portrait Modus folgen wir immer dem Roboter
                    if (follow_marker||portrait_map)
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(values[0]));
                }
            });

        }
        @Override
        protected Void doInBackground(Void... params) {
            while(true)
            {
                try {
                    LatLng new_pos = new LatLng(robot.getPosition().getLatitude(), robot.getPosition().getLongitude());
                    publishProgress(new_pos);
                    Thread.sleep(1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
