package de.dhbw.moro.communication.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

/**
 * class to connect via bluetooth
 */
public class ConnectBluetooth extends Thread {

    private BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private BluetoothAdapter bluetoothAdapter;
    private final UUID BLUETOOTH_SPP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private boolean connected = false;

    /**
     * constructor
     * @param adapter the bluetooth adapter
     * @param device the bluetooth device
     */
    ConnectBluetooth(BluetoothAdapter adapter, BluetoothDevice device) {
        bluetoothAdapter = adapter;
        mmDevice = device;
    }

    @Override
    public void run() {
        // Cancel discovery because it will slow down the connection
        bluetoothAdapter.cancelDiscovery();
        try {
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(BLUETOOTH_SPP);
        } catch (IOException ignored) {

        }
        try {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mmSocket.connect();
            Log.i("Tag", "Connected");
        } catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try {
                mmSocket.close();
            } catch (IOException ignored) {

            }
            return;
        }
        // If a connection was accepted
        if (mmSocket != null) {
            // Do work to manage the connection (in a separate thread)
            connected = true;
        }
    }

    /**
     * getter
     * @return the BluetoothSocket object
     */
    BluetoothSocket getSocket() {
        return mmSocket;
    }

    /**
     * check whether device is connected
     * @return connected?
     */
    boolean isConnected() {
        return connected;
    }

    /**
     * Will cancel an in-progress connection, and close the socket
     */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException ignored) { }
    }
}
