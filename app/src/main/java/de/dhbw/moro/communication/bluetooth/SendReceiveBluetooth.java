package de.dhbw.moro.communication.bluetooth;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * class to send a message or to receive a message via bluetooth
 */
public class SendReceiveBluetooth extends Thread{
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private final BluetoothReceiveBuffer lastStringsBuffer = new BluetoothReceiveBuffer(10);

    /**
     * constructor
     * @param socket the connected BluetoothSocket object
     */
    SendReceiveBluetooth(BluetoothSocket socket) {
        mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException ignored) {

        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;

    }

    @Override
    public void run() {
        byte[] buffer = new byte[256];  // buffer store for the stream
        int bytes; // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs
        while (true) {
            try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer);
                // Send the obtained bytes to the UI activity
               // mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                String msg= new String(buffer, 0, bytes);
                Log.i("BT antwort", msg);
                lastStringsBuffer.addString(msg);
            } catch (IOException e) {
                break;
            }
        }
    }

    /**
     * send a message via bluetooth
     * @param bytes message
     */
    public void write(byte[] bytes) {
        try {
            mmOutStream.write(bytes);
        } catch (IOException ignored) { }
    }

    /**
     * close the bluetooth connection
     */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException ignored) { }
    }

    /**
     * getter
     * @return the buffer with the last Strings
     */
    public BluetoothReceiveBuffer getLastStringsBuffer() {
        return lastStringsBuffer;
    }
}
