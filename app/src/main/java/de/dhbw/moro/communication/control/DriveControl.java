package de.dhbw.moro.communication.control;

import java.util.ArrayList;
import java.util.HashMap;

import de.dhbw.moro.communication.bluetooth.BluetoothConnection;

public class DriveControl {

    private BluetoothConnection bluetooth = BluetoothConnection.getInstance();
    private String left = null;
    private String right = null;
    private String stop = null;
    private String faw = null;
    private String baw = null;
    private String fa1 = null;
    private String ba1 = null;
    private String fa2 = null;
    private String ba2 = null;

    public DriveControl(String robot) {
        if (robot.equals("MoRo")) {
            left = "l";
            right = "r";
            stop = "s";
            faw = "v";
            baw = "z";
        }
    }

    public boolean left() {
        if (left == null){
            return false;
        }

        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String msg = left;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }

        return true;
    }

    public boolean right() {
        if (right == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String msg = right;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean forwardAllWheels(short speed) {
        if (faw == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = faw + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean backwardAllWheels(short speed) {
        if (baw == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = baw + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean forwardAxis1(byte speed) {
        if (fa1 == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = fa1 + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean backwardAxis1(byte speed) {
        if (ba1 == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = ba1 + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean forwardAxis2(byte speed) {
        if (fa2 == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = fa2 + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean backwardAxis2(byte speed) {
        if (ba2 == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String speedString;
            if (speed >= 100) {
                speedString = String.valueOf(speed);
            }
            else {
                if (speed < 10) {
                    speedString = "00" + String.valueOf(speed);
                }
                else {
                    speedString = "0" + String.valueOf(speed);
                }
            }
            String msg = ba2 + speedString;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }

    public boolean stop() {
        if (stop == null){
            return false;
        }
        if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
            String msg = stop;
            bluetooth.getSendReceiveBluetooth().write(msg.getBytes());
        }
        else {
            return false;
        }
        return true;
    }
}
