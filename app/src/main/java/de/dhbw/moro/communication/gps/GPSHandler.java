package de.dhbw.moro.communication.gps;

import android.util.Log;

import de.dhbw.moro.communication.bluetooth.BluetoothConnection;
import de.dhbw.moro.communication.bluetooth.SendReceiveBluetooth;
import de.dhbw.moro.datatypes.Position;
import de.dhbw.moro.robot.Robot;

/**
 * class to handle the GPS position
 * sends via BluetoothConnection signals to robot
 * receives position and gives it to GPSParser
 * sets Position into map
 */
public class GPSHandler extends Thread{
    private BluetoothConnection bluetooth = BluetoothConnection.getInstance();

    @Override
    public void run() {
        try
        {
            SendReceiveBluetooth sendReceiveBluetooth;
            GPSParser parser = new GPSParser();
            while (true) {
                if (bluetooth.isAdapterFound() && bluetooth.connectionEstablished()) {
                    sendReceiveBluetooth = bluetooth.getSendReceiveBluetooth();
                    sendReceiveBluetooth.start();
                    break;
                }
            }

            while (true) {
                sendReceiveBluetooth.write("p".getBytes());
                Thread.sleep(200);
                Position pos = parser.getLastPosFromBluetoothReceiveBuffer(sendReceiveBluetooth.getLastStringsBuffer());
                if (pos != null) {
                    Robot.getInstance().setPosition(pos);
                }
                Thread.sleep(500);
            }

        }
        catch(InterruptedException ex)
        {
            // Nichts tun, der Thread wurde unterbrochen
        }
    }
}
