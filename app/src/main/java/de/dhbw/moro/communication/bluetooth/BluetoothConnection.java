package de.dhbw.moro.communication.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

/**
 * class to manage the bluetooth connection
 * Singleton Pattern
 */
public class BluetoothConnection {

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice robotDevice;
    private boolean adapterFound = false;
    private ConnectBluetooth connectThread;
    private SendReceiveBluetooth sendReceiveBluetooth;
    private static BluetoothConnection instance = null;

    /**
     * private constructor - use getInstance()
     * @param robotBtName object
     */
    private BluetoothConnection(String robotBtName) {
        initialize_bluetooth();
        connectToRobot(robotBtName);
    }

    /**
     * getter
     * @return the instance
     */
    public static BluetoothConnection getInstance() {
        if (instance == null) {
            instance = new BluetoothConnection("ESP32test");
        }
        return instance;
    }

    /**
     * find the bluetooth adapter
     */
    private void initialize_bluetooth() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            adapterFound = true;
        }
        //bluetoothAdapter is null when you open the app with the emulator.
    }

    /**
     * connect to the robot
     * @param robotBtName the bluetooth name of the device
     */
    private void connectToRobot(String robotBtName) {
        robotDevice = null;

        if (bluetoothAdapter.getBondedDevices().size() > 0) {
            for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
                if (robotBtName.equals(device.getName())) {
                    robotDevice = device;
                }
            }
        }

        if (robotDevice != null) {
            connectThread = new ConnectBluetooth(bluetoothAdapter, robotDevice);
            connectThread.start();
        }
    }

    /**
     * check whether the bluetooth adapter is available
     * @return adapter available?
     */
    public boolean isAdapterFound() {
        return adapterFound;
    }

    /**
     * check whether the bluetooth connection is established
     * @return connection established?
     */
    public boolean connectionEstablished() {
        if (connectThread == null) {
            return false;
        }
        else {
            return connectThread.isConnected();
        }
    }

    /**
     * getter
     * @return object of SendReceiveBluetooth class
     */
    public SendReceiveBluetooth getSendReceiveBluetooth() {
        if (sendReceiveBluetooth == null) {
            if (isAdapterFound() && connectionEstablished()) {
                sendReceiveBluetooth = new SendReceiveBluetooth(connectThread.getSocket());
            }
        }
        return sendReceiveBluetooth;
    }
}
