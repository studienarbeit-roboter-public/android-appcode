package de.dhbw.moro.communication.gps;

import android.util.Log;

import de.dhbw.moro.communication.bluetooth.BluetoothReceiveBuffer;
import de.dhbw.moro.datatypes.Position;

/**
 * class to parse the GPS string in Position object
 */
public class GPSParser {

    /**
     * parse the position from the BluetoothReceiveBuffer
     *
     * @param buffer the BluetoothReceiveBuffer
     * @return the Position object
     */
    public Position getLastPosFromBluetoothReceiveBuffer(BluetoothReceiveBuffer buffer) {
        String string = buffer.getLastInputString();
        Log.i("GPSParser", "getLastPosition... "+string);
        if (string == null) {
            return null;
        }
        return parseGPSString(string);
    }

    /**
     * parse the position from string
     * @param string input string: e.g. "0.000000, 0.000000, 0°, 21.11.2019, 10:21:57, sats: 0"
     * @return Position object
     */
    public Position parseGPSString(String string) {
        Position result = new Position();

        String[] posArray = string.split(",");
        double lat = Double.parseDouble(posArray[0]);
        double lon = Double.parseDouble(posArray[1]);
        double course = Double.parseDouble(posArray[2].replace("°", ""));
        //course = course / 100;

        result.setLatLonCourse(lat, lon, course);
        return result;
    }
}
