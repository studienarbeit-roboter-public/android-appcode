package de.dhbw.moro.communication.bluetooth;

import java.util.ArrayList;

/**
 * class to buffer the last received strings via bluetooth
 */
public class BluetoothReceiveBuffer {
    private StringBuilder incompleteString = new StringBuilder();
    private ArrayList<String> buffer = new ArrayList<>();
    private ArrayList<String> oldBuffer = null;
    final private int lastStrings;

    /**
     * constructor
     * @param lastStrings number of last received strings to be saved
     */
    BluetoothReceiveBuffer(int lastStrings) {
        this.lastStrings = lastStrings;
    }

    /**
     * add a string fragment to buffer
     * @param string the string fragment
     */
    void addString(String string) {
        boolean nextE = checkIncompleteStringStartsWithS();
        for (int i = 0; i < string.length(); i++) {
            char thisChar = string.charAt(i);
            incompleteString.append(thisChar);
            // wenn als nächstes ein S folgen soll
            if (!nextE) {
                if (thisChar == 'S') {
                    incompleteString = new StringBuilder();
                    incompleteString.append(thisChar);
                    nextE = true;
                }
            }
            // wenn als nächstes ein E folgen soll
            else {
                if (thisChar == 'E') {
                    checkAndAddIncompleteString();
                    nextE = false;
                }
            }
        }
    }

    private String checkIncompleteString() {
        String string = incompleteString.toString();
        if (string.startsWith("S") && string.endsWith("E")) {
            return string.substring(1, string.length() -1);
        }
        else {
            return null;
        }
    }

    private boolean checkAndAddIncompleteString() {
        String s = checkIncompleteString();
        if (s != null) {
            addFullStringToBuffer(s);
            incompleteString = new StringBuilder();
            return true;
        }
        return false;
    }

    private boolean checkIncompleteStringStartsWithS() {
        String inclString = incompleteString.toString();
        if (inclString.startsWith("S")) {
            if (inclString.endsWith("E")) {
                checkAndAddIncompleteString();
            }
            return true;
        }
        return false;
    }

    /**
     * add a full string to buffer
     * @param string the string
     */
    private void addFullStringToBuffer(String string) {
        if (buffer.size() < lastStrings) {
            buffer.add(string);
        }
        else {
            oldBuffer = buffer;
            buffer = new ArrayList<>();
            buffer.add(string);
        }
    }

    /**
     * get the last strings
     * @return last received strings
     */
    public ArrayList<String> getLastInputStrings() {
        return new ArrayList<>(buffer);
    }

    /**
     * getter
     * @return the last string
     */
    public String getLastInputString() {
        if (getLastInputStrings().isEmpty()) {
            return null;
        }
        return getLastInputStrings().get(getLastInputStrings().size() - 1);
    }
}
